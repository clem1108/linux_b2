
#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp2
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;
chemin_sauvegarde= '/opt/backup/'

echo $$ | sudo tee /opt/backup.pid


touch /opt/backup/lock_$nom_site;

tar Pzcvf /opt/backup/$nom_fichier.tar.gz $parametre --ignore-failed-read > /dev/null

nombre_sauvegarde=$(ls ${chemin_sauvegarde} | grep -c ${nom_site});

