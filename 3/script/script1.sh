
#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp2
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;
chemin_sauvegarde= '/opt/backup/'

if test -z ${parametre};
then
        echo "Aucun argument n\'a été renseigné." >&2
        exit 1;
fi

if test ! -d ${chemin_sauvegarde};
then
        echo "Le chemin de sauvegarde ${chemin_sauvegarde} n\'existe pas." >&2
        exit 1;
fi

if test ! -d ${parametre};
then
        echo "Le chemin du site ${parametre} n\'existe pas." >&2;
        exit 1;
fi


if test -f /opt/backup/lock_$nom_site;
then
        echo "Script déja en cours d'exécution !";
        exit 22;
fi

