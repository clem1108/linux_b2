#!/bin/bash
# Clément Dournet
# script démarrage tp3 
# 06/10/2020

systemctl start firewalld

firewall-cmd --permanent --new-zone="custom"


echo "<?xml version='1.0' encoding='utf-8'?>
<zone target='DROP'>
  <short>Custom Zone Configuration</short>
    <description>All incomming connections are blocked by default. Only specific services are allowed.</description>
      <service name='ssh'/>
      </zone>" > /etc/firewalld/zones/custom.xml

firewall-cmd --reload

firewall-cmd --set-default-zone="custom"

firewall-cmd --reload

sudo useradd web
sudo touch /etc/systemd/system/serveur_web.service
sudo echo "
[Unit]
Description=Service Serveur Web Python2

[Service]
User=web
Environment=\"PORT=2402\"
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=\${PORT}/tcp
ExecStart=/usr/bin/python2 -m SimpleHTTPServer \${PORT}
ExecStop=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=\${PORT}/tcp

[Install]
WantedBy=multi-user.target
" >> /etc/systemd/system/serveur_web.service
sudo echo "web       ALL=(ALL)     NOPASSWD: ALL" >> /etc/sudoers
sudo systemctl enable serveur_web.service
sudo systemctl start serveur_web.service

sudo useradd backup
sudo mkdir /opt/backup
sudo touch /opt/script1.sh && sudo touch /opt/script2.sh && sudo touch /opt/script3.sh
sudo usermod -a -G web backup
sudo chown backup:backup /opt/backup && sudo chown backup:backup /opt/script1.sh && sudo chown backup:backup /opt/script2.sh && sudo chown backup:backup /opt/script3.sh
sudo echo "
#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp3 partie1
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
parametre=\$1;
nom_site=\${parametre##*/};
nom_fichier=\$nom_site'_'\$jour'_'\$heure;
chemin_sauvegarde= '/opt/backup/'
echo \$\$ | sudo tee /run/backup.pid
if test -z \${parametre};
then
        echo \"Aucun argument n\'a été renseigné.\" >&2
        exit 1;
fi

if test ! -d \${chemin_sauvegarde};
then
        echo \"Le chemin de sauvegarde \${chemin_sauvegarde} n\'existe pas.\" >&2
        exit 1;
fi

if test ! -d \${parametre};
then
        echo \"Le chemin du site \${parametre} n\'existe pas.\" >&2;
        exit 1;
fi


if test -f /opt/backup/lock_\$nom_site;
then
        echo \"Script déja en cours d'exécution !\";
        exit 22;
fi
" >> /opt/script1.sh

sudo echo "
#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp3 partie2
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
parametre=\$1;
nom_site=\${parametre##*/};
nom_fichier=\$nom_site'_'\$jour'_'\$heure;
chemin_sauvegarde= '/opt/backup/'

touch /opt/backup/lock_\$nom_site;

tar Pzcvf /opt/backup/\$nom_fichier.tar.gz \$parametre --ignore-failed-read > /dev/null

" >> /opt/script2.sh

sudo echo "
#!/bin/bash  
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp3 partie3
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;
chemin_sauvegarde= '/opt/backup/'
nombre_sauvegarde=$(ls ${chemin_sauvegarde} | grep -c ${nom_site});
chemin_lock=/opt/backup/lock_${nom_site};

if test ${nombre_sauvegarde} -ge 9;
then
        suppresion_sauvegarde=$(ls ${chemin_sauvegarde} | grep ${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf ${chemin_sauvegarde}${suppression_sauvegarde};
fi

sudo rm -rf ${chemin_lock};

" >> /opt/script3.sh

sudo touch /etc/systemd/system/backup.service
sudo echo "
[Unit]
Description=Service de backup du dossier /srv

User=backup
PIDFile=/opt/backup/backup.pid
ExecStartPre=/bin/bash /opt/script1.sh /srv
ExecStart=/bin/bash /opt/script2.sh /srv
ExecStartPost=/bin/bash /opt/script3.sh /srv" > /etc/systemd/system/backup.service
[Install]
WantedBy=multi-user.target
" >> /etc/systemd/system/backup.service

sudo echo "
backup   ALL=(ALL)   NOPASSWD:ALL
" >> /etc/sudoers

sudo echo "[Unit]
Description=Sauvegarde toutes les heures grâce au service backup.

[Service]
OnCalendar=hourly
Persistent=true

[Install]
WantedBy=timers.target" > /etc/systemd/system/backup.timer

sudo systemctl enable backup.timer
sudo systemctl start backup.timer
