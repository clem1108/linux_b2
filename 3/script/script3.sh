#!/bin/bash  
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp2
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;
chemin_sauvegarde= '/opt/backup/'
nombre_sauvegarde=$(ls ${chemin_sauvegarde} | grep -c ${nom_site});
chemin_lock=/opt/backup/lock_${nom_site};
if test ${nombre_sauvegarde} -ge 9;
then
        suppresion_sauvegarde=$(ls ${chemin_sauvegarde} | grep ${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf ${chemin_sauvegarde}${suppression_sauvegarde};
fi

echo $chemin_lock

sudo rm -rf ${chemin_lock};


