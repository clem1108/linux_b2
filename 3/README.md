# TP3 : systemd
Voici le `VagrantFile` que j'ai utilisé pour cette première partie : 
<a href="Vagrant/Vagrantfile">VagrantFile centos 7</a>
## I. Services systemd
### 1. Intro

Nous allons afficher le nombre de services `systemd` disponible sur la machine à l'aide de la commande :

```bash
[vagrant@node1 ~]$ sudo systemctl -t service -all | grep -c service
105
```
Nous constatons donc qu'il y en a 105.

Ensuite, affichons le nombre de services en cours d'exécution : 
```bash
[vagrant@node1 ~]$ sudo systemctl -t service | grep -c running
16
```
Il y en a 16.

Passons à ceux qui sont inacitfs ou dont le lancement a échoué : 

```bash
[vagrant@node1 ~]$ sudo systemctl -t service | grep -c -e failed -e exited
17
```
17 services sont inactifs.

Enfin, passons à ceux qui se lancent au démarrage : 

```bash
[vagrant@node1 ~]$ sudo systemctl list-unit-files -t service | grep -c enabled
30
```

Il y a donc 30 services qui se lancent automatiquement au démarrage.


### 2. Analyse d'un service

Nous allons étudier le fichier de configuration de l'unité `systemd` du service `nginx`qui est un service permettant de créer un serveur web.

```bash
[vagrant@node1 ~]$ systemctl cat nginx
# /usr/lib/systemd/system/nginx.service
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
# Nginx will fail to start if /run/nginx.pid already exists but has the wrong
# SELinux context. This might happen when running `nginx -t` from the cmdline.
# https://bugzilla.redhat.com/show_bug.cgi?id=1268621
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
Le `PATH` du fichier de configuration du service se situe à l'emplacement `/usr/lib/systemd/system/nginx.service`.
Le fichier est composé de trois grandes parties : 
* La première partie s'appelle `Unit`. Elle contient la description du service, ainsi que les noms des services qui doivent être d'abord lancé avant celui ci.
* La deuxième partie est `Service`. Elle est composée de son type, indique l'emplacement du fichier contenant son PID, les actions avant son lancement, pendant le lancement, après le lancement et lorsque l'on redémarre le service, et enfin les actions lancées durant l'extinction du service.
* La dernière partie est `Install`. Elle permet d'activer le démarrage automatique du service au démarrage du système.

Listons tous les services dont le fichier de configuration contient la ligne `WantedBy=multi-user.target`: 
```bash
[vagrant@node1 ~]$ grep -r "WantedBy=multi-user.target" /etc/systemd/system/* /run/systemd/system/* /usr/lib/systemd/system/*| cut -d':' -f1 | cut -d'/' -f5,6 | cut -d'/' -f2
NetworkManager.service
auditd.service
brandbot.path
chrony-wait.service
chronyd.service
cpupower.service
crond.service
ebtables.service
firewalld.service
fstrim.timer
gssproxy.service
irqbalance.service
machines.target
nfs-client.target
nfs-rquotad.service
nfs-server.service
nfs.service
nginx.service
postfix.service
rdisc.service
remote-cryptsetup.target
remote-fs.target
rhel-configure.service
rpc-rquotad.service
rpcbind.service
rsyncd.service
rsyslog.service
sshd.service
tcsd.service
tuned.service
vmtoolsd.service
wpa_supplicant.service
```
Nous pouvons donc constater que de nombreux services contiennent cette ligne, et donc qu'ils se lancent potentiellement de manière automatique au démarrage de la machine.

### 3. Création d'un service

Afin de procéder à la création de ces services, j'ai créé un script qui s'exécute au démarrage et fait la configuration au démarrage des différensts services.

<a href="script/Script_demarrage_tp3.sh">Script démarrage</a>

#### A. Serveur web

Voici le fichier contenant l'unité de service su serveur web :
```bash
[vagrant@node1 ~]$ sudo cat /etc/systemd/system/serveur_web.service 

[Unit]
Description=Service Serveur Web Python2

[Service]
User=web
Environment="PORT=2402"
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=${PORT}/tcp
ExecStart=/usr/bin/python2 -m SimpleHTTPServer ${PORT}
ExecStop=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=${PORT}/tcp

[Install]
WantedBy=multi-user.target
```
Lors du démarrage du service, le port `2402` s'ouvre afin d'autoriser la connexion. Ensuite le service se lance en tâche de fond. Si l'on souhaite arréter le service, le prt se referme afin que la connexion ne soit plus autorisée.

<a href="units/serveur_web.service">Fichier serveur web</a>

Afin que ce service soit fonctionnel, il faut d'abord créer un utilisateur relatif à ce service :
```bash
[vagrant@node1 ~]$ sudo useradd web
```
Ensuite, il faut configurer cet utilisateur en tant qu'administrateur sans le besoin de taper son mot de passe : 
```bash
sudo echo "web       ALL=(ALL)     NOPASSWD: ALL" >> /etc/sudoers
```

Il est maintenant nécessaire de demander la relecture du fichier de configuration, l'activer au démarrage et vérifier si tout fonctionne : 
```bash
[vagrant@node1 ~]$ sudo systemctl daemon-reload
[vagrant@node1 ~]$ sudo systemctl enable serveur_web.service
Created symlink from /etc/systemd/system/multi-user.target.wants/serveur_web.service to /etc/systemd/system/serveur_web.service.
[vagrant@node1 ~]$ sudo systemctl start serveur_web.service 
[vagrant@node1 ~]$ sudo systemctl status serveur_web.service
● serveur_web.service - Service Serveur Web Python2
   Loaded: loaded (/usr/lib/systemd/system/serveur_web.service; static; vendor preset: disabled)
      Active: active (running) since Wed 2020-10-07 07:18:40 UTC; 5s ago
       Main PID: 2288 (python2)
          CGroup: /system.slice/serveur_web.service
	             └─2288 /usr/bin/python2 -m SimpleHTTPServer 2402

		     Oct 07 07:18:40 node1.tp3.b2 systemd[1]: [/usr/lib/systemd/system/serveur_web.service:7] Executable path is not ...anent
		     Oct 07 07:18:40 node1.tp3.b2 systemd[1]: [/usr/lib/systemd/system/serveur_web.service:9] Executable path is not ...anent
		     Oct 07 07:18:40 node1.tp3.b2 systemd[1]: Started Service Serveur Web Python2.
		     Hint: Some lines were ellipsized, use -l to show in full.
```
Le service s'est donc lancé sans erreur, maintenant essayons d'utiliser la commande `curl` depuis ma machine `hôte` : 
```bash
clement@lenovo:~$ curl 192.168.3.31:2402
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="swapfile">swapfile</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
</ul>
<hr>
</body>
</html>
```

J'arrive donc à récupérer la page web, ce service est donc fonctionnel.

#### B. Sauvegarde

Passons maintenant à la création d'une unité `systemd` pour la réalisation d'une sauvegarde.
Voici le contenu du fichier de l'unité : 
```bash
[vagrant@node1 ~]$ sudo cat /etc/systemd/system/backup.service      

[Unit]
Description=Service de backup du dossier /srv

[Service]
User=backup
PIDFile=/opt/backup.pid
ExecStartPre=/bin/bash /opt/script1.sh /srv
ExecStart=/bin/bash /opt/script2.sh /srv
ExecStartPost=/bin/bash /opt/script3.sh /srv
[Install]
WantedBy=multi-user.target
```

<a href="units/backup.service">Unité backup</a>

Ce fichier s'exécute donc avbec l'utilisateur `backup`, utilise le fichier `PID` se trouvant à l'emplacement `/opt/backup.pid`.

Afin de pouvoir créer des actions pré-lancement, au lancement et post-lancement, le script a été éclaté en trois scripts différents. Voici le premier script : 
```bash
[vagrant@node1 ~]$ sudo cat /opt/script1.sh 

#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp3 partie1
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;
chemin_sauvegarde= '/opt/backup/'

if test -z ${parametre};
then
        echo "Aucun argument n\'a été renseigné." >&2
        exit 1;
fi

if test ! -d ${chemin_sauvegarde};
then
        echo "Le chemin de sauvegarde ${chemin_sauvegarde} n\'existe pas." >&2
        exit 1;
fi

if test ! -d ${parametre};
then
        echo "Le chemin du site ${parametre} n\'existe pas." >&2;
        exit 1;
fi


if test -f /opt/backup/lock_$nom_site;
then
        echo "Script déja en cours d'exécution !";
        exit 22;
fi
``` 
<a href="script/script1.sh"> Script backup partie 1</a>

Ce script vérifie si le path est bien renseigné, si le chemin de destination existe, si le chemin du site existe et si le script n'est pas déjà en cours d'exécution.

Passons au deuxième script : 
```bash
[vagrant@node1 ~]$ sudo cat /opt/script2.sh 

#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp3 partie2
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;
chemin_sauvegarde= '/opt/backup/'

echo $$ | sudo tee /opt/backup.pid


touch /opt/backup/lock_$nom_site;

tar Pzcvf /opt/backup/$nom_fichier.tar.gz $parametre --ignore-failed-read > /dev/null

```

<a href="script/script2.sh">Script backup partie 2</a>

Ce script s'occupe de la réalisation de la sauvegarde.

Passons au dernier script :

```bash
[vagrant@node1 ~]$ sudo cat /opt/script3.sh
#!/bin/bash  
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp3 partie3
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;
chemin_sauvegarde= '/opt/backup/'
nombre_sauvegarde=$(ls ${chemin_sauvegarde} | grep -c ${nom_site});
chemin_lock=/opt/backup/lock_${nom_site};

if test ${nombre_sauvegarde} -ge 9;
then
        suppresion_sauvegarde=$(ls ${chemin_sauvegarde} | grep ${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf ${chemin_sauvegarde}${suppression_sauvegarde};
fi

sudo rm -rf ${chemin_lock};
```

<a href="script/script3.sh">Script backup partie 3</a>

Ce script compte le nombre de sauvegarde effectué, efface les sauvegardes supplémentaires lorsqu'il y en a plus de 7 et efface le fichier permettant de savoir si le script est déjà en cours d'exécution.

Afin d'automatiquer le lancement de ce script toutes les heures, il est nécessaire de créer le fichier `backup.timer` que voici : 

```bash
[vagrant@node1 ~]$ sudo cat /etc/systemd/system/backup.timer 
[Unit]
# Permet de rédiger la description du timer backup.
Description=Sauvegarde toutes les heures grace au service backup

[Timer]
# Indique que l'on veut que la répétion du timer soit toutes les heures.
OnCalendar=hourly
# Permet de lancer le service si la dernière exécution a été manquer.
Persistent=true

[Install]
# Permet d'indiquer que le timer a la possibilité d'être lancé au démarrage de la machine.
WantedBy=timers.target
```

<a href="units/backup.timer">Backup.timer </a>

Ce fichier indique donc que toutes les heures le service `backup.service` doit s'exécuter.

Afin de rendre tout cela possible, il est nécessaire de créer un user `backup` et de lui donner les differentes autorisations : 

```bash
[vagrant@node1 ~]$ sudo useradd backup
[vagrant@node1 ~]$ sudo usermod -a -G web backup
[vagrant@node1 ~]$ sudo chown backup:backup /opt/backup && [vagrant@node1 ~]$ sudo chown backup:backup /opt/script1.sh && sudo chown backup:backup /opt/script2.sh && sudo chown backup:backup /opt/script3.sh
``` 

On l'attribue au groupe ̀`web` pour qu'il puisse intéragir avec les dossiers présents dans `/srv` appartenant à `web`.

Il faut donc maintenant automatiser le démarrage et le lancer : 
```bash
[vagrant@node1 ~]$ sudo systemctl enable backup.timer
Created symlink from /etc/systemd/system/timers.target.wants/backup.timer to /etc/systemd/system/backup.timer.
[vagrant@node1 ~]$ sudo systemctl start backup.timer
```

Le système de sauvegarde s'exécute donc de manière automatique à l'aide d'une unité `systemd`.

## 2. Autre features 
Afin de réaliser cette partie, il est nécassaire d'utiliser une nouvelle box. Voici donc le nouveau fichier de configuration : 
<a href="Vagrant/Vagrantfile_centos8">VagrantFile centos 8</a>

### 1. Gestion de boot
j'exécute donc la commande me permettant de génerer un fichier svg que voici : 
<a href="startup.svg">graphe du boot</a>

Nous pouvons donc remarquer que les trois services les plus long à démarrer sont : 
* tuned.service
* sshd-keygen@rsa.service
* sssd.service

### 2. Gestion de l'heure

Vérifions le fuseau horaire à l'aide de cette commande :
```bash
[vagrant@node2 ~]$ timedatectl
               Local time: Fri 2020-10-09 19:32:23 UTC
           Universal time: Fri 2020-10-09 19:32:23 UTC
                 RTC time: Fri 2020-10-09 19:32:22
                Time zone: UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
``` 

Nous pouvons donc constater que le fuseau horaire est `UTC +0000`. Cette machine est synchronisée avec un serveur NTP.

Vérifions s'il existe un fuseau horaire pour Paris : 
```bash
[vagrant@node2 ~]$ timedatectl list-timezones | grep Paris
Europe/Paris
``` 

Comme celui-ci existe, basculons sur le bon fuseau horaire : 
```bash
[vagrant@node2 ~]$ sudo timedatectl set-timezone Europe/Paris
[vagrant@node2 ~]$ timedatectl
               Local time: Fri 2020-10-09 21:34:00 CEST
           Universal time: Fri 2020-10-09 19:34:00 UTC
                 RTC time: Fri 2020-10-09 19:33:59
                Time zone: Europe/Paris (CEST, +0200)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

Nous pouvons donc constater que la modification a bien été prise en compte.


### 3. Gestion des noms et de la résolution de noms

Déterminons le `hostname` actuel de la machine : 
```bash
[vagrant@node2 ~]$ hostnamectl
   Static hostname: node2.tp3.b2
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 3072b853541947de965c59763ab82f9e
           Boot ID: 5b5892066a3e4559991ed3255e81295b
    Virtualization: oracle
  Operating System: CentOS Linux 8 (Core)
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-80.el8.x86_64
      Architecture: x86-64
```

Cette machine s'appelle donc `node2.tp3.b2`.

Changeons le `hostname` de la machine en tapant la commande suivante : 
```bash
[vagrant@node2 ~]$ sudo hostnamectl set-hostname centos8
[vagrant@node2 ~]$ hostnamectl
   Static hostname: centos8
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 3072b853541947de965c59763ab82f9e
           Boot ID: 5b5892066a3e4559991ed3255e81295b
    Virtualization: oracle
  Operating System: CentOS Linux 8 (Core)
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-80.el8.x86_64
      Architecture: x86-64
```

Nous remarquons que la machine s'appelle désormais `centos8`.