# TP2 : Déploiement automatisé
## Déploiement simple
Afin de réaliser cette partie, j'ai créé un `VagrantFile`. Ce fichier permet la configuration des VMs à créer.

Voici le fichier de configuration pour cette partie : 
```ruby
PS C:\Users\cleme\Documents\vagrant> cat .\Vagrantfile
file_to_disk = 'VagrantDisk2.vdi'
Vagrant.configure("2")do|config|
	config.vm.box="centos/7"
	config.vm.network "private_network", ip: "192.168.2.11/24"
	config.vm.hostname = "vm1.tp2.b2"
	config.vbguest.auto_update = false
	config.vm.synced_folder ".", "/vagrant", disabled: true
	config.vm.provision "shell", path: "script_install_vim.sh"
	config.vm.provider "virtualbox" do |v|
		v.name = "vm1.tp2.b2"
		v.memory = 1024
		v.customize ['createhd', '--filename', file_to_disk, '--size', 5 * 1024]
		 v.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
		end
	end
```

Nous pouvons constater que la mémoire allouée est définie par cette ligne `v.memory = 1024`, l'IP statique `config.vm.network "private_network", ip: "192.168.2.11/24"`, le nom interne `v.name = "vm1.tp2.b2"` et le hostname `config.vm.hostname = "vm1.tp2.b2"`.

Par la suite, j'ai rajouté la ligne `config.vm.provision "shell", path: "script_install_vim.sh"`, qui va permettre d'exécuter le script au démarrage qui installera le paquet `vim` : 
```bash
PS C:\Users\cleme\Documents\vagrant> cat .\script_install_vim.sh
#!/bin/bash

sudo yum install -y vim
```
* <a href="part1/script_install_vim.sh">Script n°1 </a>

Afin de rajouter un deuxième disque de 5 go, j'ai ajouté cette succession de lignes : 
```ruby
file_to_disk = 'VagrantDisk2.vdi'
v.customize ['createhd', '--filename', file_to_disk, '--size', 5 * 1024]
v.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
```

* <a href="part1/Vagrantfile">VagrantFile partie 1 </a>

Ces lignes permettent de nommer le disque et de définir les différentes options le concernant.

Pour pouvoir démarrer sur ce fichier de configuration, il est nécessaire de taper la commande `vagrant up`, et le logiciel s'occupera de la création , de la configuration et du démarrage de la VM.

## 3 Multi-node deployment

Afin de créer deux VMs en même temps, il est nécessaire de créer deux parties distinctes dans le `VagrantFile`. Voici le miens :  

```ruby
Vagrant.configure("2") do |config|
  # Configuration commune à toutes les machines
    config.vm.box = "b2-tp2-centos"
    config.vbguest.auto_update = false
    config.vm.box_check_update = false 
    config.vm.synced_folder ".", "/vagrant", disabled: true
  # Config une première VM "node1"
    config.vm.define "node1" do |node1|
  # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
        node1.vm.network "private_network", ip: "192.168.2.21"
        node1.vm.hostname = "node1.tp2.b2"
        node1.vm.provider "virtualbox" do |v1|
            v1.name = "node1"
            v1.memory = 1024
            end
     end
  # Config une première VM "node2"
    config.vm.define "node2" do |node2|
  # remarquez l'utilisation de 'node2.' défini sur la ligne au dessus
        node2.vm.network "private_network", ip: "192.168.2.22"
        node2.vm.hostname = "node2.tp2.b2"
        node2.vm.provider "virtualbox" do |v2|
            v2.name = "node2"
            v2.memory = 512
            end
        end
end
```
Afin qu'elles démarrent sur le paquet repackagé, il est nécessaire d'écrire cette ligne : `config.vm.box = "b2-tp2-centos"`. Elle indique sur quelle base le logiciel doit créer les VMs.
Nous pouvons voir les deux parties. Elles sont chacune nommées avec le nom de la machine qu'elle pilote. Exemple : `node1.vm.network "private_network", ip: "192.168.2.21"` veut dire que cette ligne configure une IP fixe sur le reséau `host-only` sur la `node 1`.

* <a href="part3/Vagrantfile">VagrantFile partie 3 </a>


## IV. Automation here we (slowly) come

Afin de réaliser cette dernière partie, j'ai réutilisé le fichier `VagrantFile` réalisé précédemment et je lui ai apporté quelques modifications afin qu'il respecte la consigne : 
```ruby
Vagrant.configure("2") do |config|
  # Configuration commune à toutes les machines
    config.vm.box = "b2-tp2-centos"
    config.vbguest.auto_update = false
    config.vm.box_check_update = false 
    config.vm.synced_folder ".", "/vagrant", disabled: true

  # Config une première VM "node1"
    config.vm.define "node1" do |node1|
  # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
        node1.vm.network "private_network", ip: "192.168.2.21"
        node1.vm.provision "shell", path: "script_demarrage_node1.sh"
	node1.vm.hostname = "node1.tp2.b2"
        node1.vm.provider "virtualbox" do |v1|
            file_to_disk = 'VagrantDisk2_node1.vdi'
	    v1.name = "node1"
            v1.memory = 1024
            v1.customize ['createhd', '--filename', file_to_disk, '--size', 5 * 1024]
	    v1.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
	    end
     end
     
     # Config une première VM "node2"
    config.vm.define "node2" do |node2|
  # remarquez l'utilisation de 'node2.' défini sur la ligne au dessus
        node2.vm.network "private_network", ip: "192.168.2.22"
	node2.vm.provision "shell", path: "script_demarrage_node2.sh"
        node2.vm.hostname = "node2.tp2.b2"
        node2.vm.provider "virtualbox" do |v2|
            file_to_disk = 'VagrantDisk2_node2.vdi'
	    v2.name = "node2"
            v2.memory = 512
	    v2.customize ['createhd', '--filename', file_to_disk, '--size', 5 * 1024]
            v2.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd',     '--medium', file_to_disk]
	    end
        end
 end
```

* <a href="part4/Vagrantfile">VagrantFile partie 4 </a>

Afin de configurer les machines comme pour le TP1, j'ai réalisé un script pour les deux machines.

Commençons par celui sur le `node1` :
```bash
#!/bin/bash
# Clément DOURNET
# 30/09/2020
# script demarrage node 1

# configuration de base

echo "192.168.2.22 node2.tp2.b2" >> /etc/hosts
sudo useradd admin
sudo usermod -aG wheel admin
echo -e "admin\nadmin\n" | sudo passwd admin

# configuration serveur web
sudo useradd web
sudo mkdir /srv/site1
sudo mkdir /srv/site2
sudo touch /srv/site1/index.html
echo "COUCOU !! FROM SITE1" > /srv/site1/index.html
sudo touch /srv/site2/index.html
echo -e "COUCOU !! FROM SITE2" > /srv/site2/index.html
sudo rm /etc/nginx/nginx.conf
sudo touch /etc/nginx/nginx.conf
# fichier de configuration préparé préalablement
echo "#   * Official Russian Documentation: http://nginx.org/ru/docs/

worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;
events {
    worker_connections 1024;
}

http {
        server {
                listen 80;
                server_name node1.tp2.b2;

                location / {
                        return 301 /site1;
                }

                location /site1 {
                        alias /srv/site1/index.html;
                }

                location /site2 {
                        alias /srv/site2/index.html;
                }
        }
        server {
                listen 443 ssl;
                ssl_certificate /etc/pki/tls/certs/server.crt;
                ssl_certificate_key /etc/pki/tls/private/server.key;
                server_name node2.tp1.b2;

                location / {
                        return 301 /site1;
                }

                location /site1 {
                        alias /srv/site1/index.html;
                }

                location /site2 {
                        alias /srv/site2/index.html;
                }
        }
} " >> /etc/nginx/nginx.conf

# certificat généré préalablement
sudo touch /etc/pki/tls/certs/server.crt
sudo touch /etc/pki/tls/private/server.key
sudo chmod 400 /etc/pki/tls/certs/server.crt && sudo chmod 400 /etc/pki/tls/certs/server.crt
sudo echo "-----BEGIN CERTIFICATE-----
MIIDqTCCApGgAwIBAgIJAOnb70Ps9tYwMA0GCSqGSIb3DQEBCwUAMGsxCzAJBgNV
BAYTAkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDEN
MAsGA1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRw
Mi5iMjAeFw0yMDEwMDQxMjUzMjFaFw0yMTEwMDQxMjUzMjFaMGsxCzAJBgNVBAYT
AkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDENMAsG
A1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRwMi5i
MjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMi6v6vXOlLAwt7pvb9w
WdCD2AeBHgwFlIURrU13zdfmlDsTUS5dEqgIVxeKnEsoSGwyMLawcZ53kzvE6lxO
O5J6uFXKeq+Wk7NSFtHE71Gf5yWy0ZAyHJ4w+M5jn2EtzuOs4Z39lqjSWTFnvUtD
JigxuQvqRT1NWj844pLhZ5FoK1N96jfA/nFMRwpGf8TepfozoGuoll2cbyPmzrmM
iS6PR9IQRJ7VTjQDtjXjBCbaphCzn6azllsF1a/gi+NbDnkloHcMQ/np3odvlXDw
dlF+t7RPT4z0qEzswKN8mpW5mLtnU+lCEeHJLv4GMBFnhohoFeshv15FL7biFy5Y
vN0CAwEAAaNQME4wHQYDVR0OBBYEFF14NHhFnhi9aEK2C9rUyidj0D6bMB8GA1Ud
IwQYMBaAFF14NHhFnhi9aEK2C9rUyidj0D6bMAwGA1UdEwQFMAMBAf8wDQYJKoZI
hvcNAQELBQADggEBALsmo/oj9aXv5FxJotJNaycLnlDxiCtaPS66gS1KokYaKxMZ
dGT6gyGWJTDf2abuD3Bo1AGt2RnauboFMFILGDt6flhCSpipLmFZC0aAjfDSJfzd
ZEXmgHKmoqEQLKx0gglGCgZIAhWo3Hm67VItXn31lwe1UO4yNsHl/eiHQDuph1aq
ZI/XwxQH26FshrEerzJRAvGityuplOWOXmy9bkdOD2c2Pdz2ix28bOxUIAhWC5HM
9SBCRhgtaXM0wQdMeJbnwvW4ToWcsP4SD/Ff4esX1ZLsW//tro34v4N5yL1LSU6X
fR6ofRuEanOcJ+FSxuwN9HSEOF45tVJnJW5CHMg=
-----END CERTIFICATE-----
" >> /etc/pki/tls/certs/server.crt
echo "-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDIur+r1zpSwMLe
6b2/cFnQg9gHgR4MBZSFEa1Nd83X5pQ7E1EuXRKoCFcXipxLKEhsMjC2sHGed5M7
xOpcTjuSerhVynqvlpOzUhbRxO9Rn+clstGQMhyeMPjOY59hLc7jrOGd/Zao0lkx
Z71LQyYoMbkL6kU9TVo/OOKS4WeRaCtTfeo3wP5xTEcKRn/E3qX6M6BrqJZdnG8j
5s65jIkuj0fSEESe1U40A7Y14wQm2qYQs5+ms5ZbBdWv4IvjWw55JaB3DEP56d6H
b5Vw8HZRfre0T0+M9KhM7MCjfJqVuZi7Z1PpQhHhyS7+BjARZ4aIaBXrIb9eRS+2
4hcuWLzdAgMBAAECggEBAJyTV4nClGMSq69YIsPe7qBBB9UquXZlL+nw1P81saJ0
4TGHqu6AeVoLT2w0mh/iJ0QGjosi7S3QVk/sFWe1OrDS+1EoZTTsPwBEyzoL5SYF
ljeI2gEhpcMbrijH+v3Bzr04ELG9qrS7iAmKw1Se53e4+YA62JGt8fSY+dva6X+4
n5BzI0kTihmgRc9kW/Gu73eh7y0SZRnvk0d5z51/v9kxeD1Np4Mj56MfLF1Ny+Qg
bHxTCsRXPtFLTvGjkJnsPGMoazf+QU1dFmELWUKl0JuqkXuPidBmCb23GHug9aIS
F9DbKNUylu+ZRCi4cpXC0Q4ITnyoPxVN2s3tVwm5m70CgYEA+oBVbkAXo1s9A1m0
XGvo6J8hAt5equqrLFeYT2W8wPgwRXO9cM/i+Ez4HAFVd49ytFaAzlylf6M5dfib
SjZFocWocqHYBNyRP7xeK8Ojk2rWaTK3XcZXJRWpCwcQJazG4FG7s+HwFjqCLEXU
srm0swc5dkXvxoTgTBFyGg1KXdMCgYEAzSK6NyZx0bkwgOaeT2e/k0m6nFCh3puG
01xiLBo4kShgwTlcrvpC6Tb1pqwRB/UIryfnt1jq+9lLXReU42IYAxYRKl2MDjiZ
7WYbzWQNTAQKuk/aB49M+Pvb0CiK6tjGSEj3Dtzh9aIZL6YeDZgXRF/hAZoDqCq9
a5jbeYz/3I8CgYByWPqmZSNlMlTbnqsyNSAQb/NFt/RG2PBiQWoJdEuBWsnOKlMj
mAHnjlsDjahR921I3OPXT3tMlFm6fE2GxtdqTSvjh/Vv4q97BKWIHLoipPZS9w+1
AsDs8MJFgmfgisoNw8GHD2/k9bhONWIuY48U2gtk7e/SEv3AO4Z2VfLIeQKBgE/Y
VY0D3MeaAoQZv8L+ifXle8o+JFNGTBtW2St7ZUsy4T40epvs80BdUQ536QUk3iV3
Ld0SshwnPwtUE3LSCrCg1TudP9eFNnzOAsJWUEqnRBmy3RWWNIqxVaTXMwMoLz2R
kQBq/1ShrJX8XbRC38OQYj9CWGwLfsEztTHBrRGHAoGAUdFQN9fU4RAQngWsUR6z
aFwX/4Kv+xupZIR/yTBxXDix3sN1hE048SkRPmxEItwtDMjmt5W/z/3nptugGUR+
Tbf/VcjsS/Daz4+XUTVn8S1W8LoN68MBckdL2VSqdBzkNcjsCQlg1yHK4IIlBwTJ
MUNOLj+UbnbEJFtcOkmeHDc=
-----END PRIVATE KEY-----
" >> /etc/pki/tls/private/server.key
sudo chown web:web /srv/site*
sudo chown web:web /srv/site1/*
sudo chown web:web /srv/site2/*
sudo chmod 744 /srv/site*
sudo firewall-cmd --add-port=80/tcp --permanent && sudo firewall-cmd --add-port=443/tcp --permanent
sudo firewall-cmd --reload 
sudo systemctl start nginx

# Sauvegarde

sudo touch /opt/tp2_backup.sh
sudo useradd backup
sudo usermod -a -G web backup
sudo mkdir /opt/backup
sudo chown backup:backup /opt/backup && sudo chown backup:backup /opt/tp2_backup.sh
sudo echo "
#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp2 
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
parametre=\$1;
nom_site=\${parametre##*/};
nom_fichier=\$nom_site'_'\$jour'_'\$heure;

if test -f /opt/backup/lock_\$nom_site;
then
        echo 'Script déja en cours d exécution !';
        exit 22;
fi


touch /opt/backup/lock_\$nom_site;

tar Pzcvf /opt/backup/\$nom_fichier.tar.gz \$parametre

nombre_fichiers=\$(ls /opt/backup | grep -c \$nom_site);

if test \$nombre_fichiers -ge 9;
then
        suppression=\$(ls /opt/backup/ | grep \$nom_site | sort | head -n 2 | tail -n 1);
        rm -rf /opt/backup/\$suppression;
fi

rm /opt/backup/lock_\$nom_site;

" >> /opt/tp2_backup.sh
sudo echo "
0  *  *  *  * backup     bash /opt/tp2_backup.sh /srv/site1
0  *  *  *  * backup     bash /opt/tp2_backup.sh /srv/site2
" >> /etc/crontab


# Netdata

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;


sudo echo "
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/761275561540190228/CQuKu4Y1-rCbeQrl-7DCkuf5nFCcbJS2FMulTZjOMwbbQ_cbj4tNxmDh05lG2F6PkjqZ"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
" >> /etc/netdata/health_alarm_notify.conf
``` 

* <a href="part4/script_demarrage_node1.sh">Script démarrage node 1 </a>

Ce script copie donc les fichiers de configuration nécessaires aux différents services (serveur web), un certificat et sa clé pour le serveur web, crée et automatise un système de sauvegarde à l'aide d'un script, exécute la commande installant netdata (service de monitoring) et configure la partie notification sur Discord.

Maintenant, passons à celui sur le `node2` : 
```bash
#!/bin/bash
#Clément DOURNET
#01/10/2020
# script démarrage node 2

# COnfiguration de base
echo "192.168.2.21 node1.tp2.b2" >> /etc/hosts
sudo useradd admin
sudo usermod -aG wheel admin
echo -e "admin\nadmin\n" | sudo passwd admin

# copie du certificat 

sudo touch /usr/share/pki/ca-trust-source/server.crt
sudo echo "-----BEGIN CERTIFICATE-----
MIIDqTCCApGgAwIBAgIJAOnb70Ps9tYwMA0GCSqGSIb3DQEBCwUAMGsxCzAJBgNV
BAYTAkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDEN
MAsGA1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRw
Mi5iMjAeFw0yMDEwMDQxMjUzMjFaFw0yMTEwMDQxMjUzMjFaMGsxCzAJBgNVBAYT
AkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDENMAsG
A1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRwMi5i
MjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMi6v6vXOlLAwt7pvb9w
WdCD2AeBHgwFlIURrU13zdfmlDsTUS5dEqgIVxeKnEsoSGwyMLawcZ53kzvE6lxO
O5J6uFXKeq+Wk7NSFtHE71Gf5yWy0ZAyHJ4w+M5jn2EtzuOs4Z39lqjSWTFnvUtD
JigxuQvqRT1NWj844pLhZ5FoK1N96jfA/nFMRwpGf8TepfozoGuoll2cbyPmzrmM
iS6PR9IQRJ7VTjQDtjXjBCbaphCzn6azllsF1a/gi+NbDnkloHcMQ/np3odvlXDw
dlF+t7RPT4z0qEzswKN8mpW5mLtnU+lCEeHJLv4GMBFnhohoFeshv15FL7biFy5Y
vN0CAwEAAaNQME4wHQYDVR0OBBYEFF14NHhFnhi9aEK2C9rUyidj0D6bMB8GA1Ud
IwQYMBaAFF14NHhFnhi9aEK2C9rUyidj0D6bMAwGA1UdEwQFMAMBAf8wDQYJKoZI
hvcNAQELBQADggEBALsmo/oj9aXv5FxJotJNaycLnlDxiCtaPS66gS1KokYaKxMZ
dGT6gyGWJTDf2abuD3Bo1AGt2RnauboFMFILGDt6flhCSpipLmFZC0aAjfDSJfzd
ZEXmgHKmoqEQLKx0gglGCgZIAhWo3Hm67VItXn31lwe1UO4yNsHl/eiHQDuph1aq
ZI/XwxQH26FshrEerzJRAvGityuplOWOXmy9bkdOD2c2Pdz2ix28bOxUIAhWC5HM
9SBCRhgtaXM0wQdMeJbnwvW4ToWcsP4SD/Ff4esX1ZLsW//tro34v4N5yL1LSU6X
fR6ofRuEanOcJ+FSxuwN9HSEOF45tVJnJW5CHMg=
-----END CERTIFICATE-----
" >> /usr/share/pki/ca-trust-source/server.crt
```
Ce script fait la configuration de base et copie le certificat afin que celui-ci soit utilisé lorsque la machine souhaite accéder au serveur web de manière sécurisée.

* <a href="part4/script_demarrage_node2.sh">Script démarrage node 2 </a>
