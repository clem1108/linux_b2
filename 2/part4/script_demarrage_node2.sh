#!/bin/bash
#Clément DOURNET
#01/10/2020
# script démarrage node 2

# COnfiguration de base
echo "192.168.2.21 node1.tp2.b2" >> /etc/hosts
sudo useradd admin
sudo usermod -aG wheel admin
echo -e "admin\nadmin\n" | sudo passwd admin

# copie du certificat 

sudo touch /usr/share/pki/ca-trust-source/server.crt
sudo echo "-----BEGIN CERTIFICATE-----
MIIDqTCCApGgAwIBAgIJAOnb70Ps9tYwMA0GCSqGSIb3DQEBCwUAMGsxCzAJBgNV
BAYTAkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDEN
MAsGA1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRw
Mi5iMjAeFw0yMDEwMDQxMjUzMjFaFw0yMTEwMDQxMjUzMjFaMGsxCzAJBgNVBAYT
AkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDENMAsG
A1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRwMi5i
MjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMi6v6vXOlLAwt7pvb9w
WdCD2AeBHgwFlIURrU13zdfmlDsTUS5dEqgIVxeKnEsoSGwyMLawcZ53kzvE6lxO
O5J6uFXKeq+Wk7NSFtHE71Gf5yWy0ZAyHJ4w+M5jn2EtzuOs4Z39lqjSWTFnvUtD
JigxuQvqRT1NWj844pLhZ5FoK1N96jfA/nFMRwpGf8TepfozoGuoll2cbyPmzrmM
iS6PR9IQRJ7VTjQDtjXjBCbaphCzn6azllsF1a/gi+NbDnkloHcMQ/np3odvlXDw
dlF+t7RPT4z0qEzswKN8mpW5mLtnU+lCEeHJLv4GMBFnhohoFeshv15FL7biFy5Y
vN0CAwEAAaNQME4wHQYDVR0OBBYEFF14NHhFnhi9aEK2C9rUyidj0D6bMB8GA1Ud
IwQYMBaAFF14NHhFnhi9aEK2C9rUyidj0D6bMAwGA1UdEwQFMAMBAf8wDQYJKoZI
hvcNAQELBQADggEBALsmo/oj9aXv5FxJotJNaycLnlDxiCtaPS66gS1KokYaKxMZ
dGT6gyGWJTDf2abuD3Bo1AGt2RnauboFMFILGDt6flhCSpipLmFZC0aAjfDSJfzd
ZEXmgHKmoqEQLKx0gglGCgZIAhWo3Hm67VItXn31lwe1UO4yNsHl/eiHQDuph1aq
ZI/XwxQH26FshrEerzJRAvGityuplOWOXmy9bkdOD2c2Pdz2ix28bOxUIAhWC5HM
9SBCRhgtaXM0wQdMeJbnwvW4ToWcsP4SD/Ff4esX1ZLsW//tro34v4N5yL1LSU6X
fR6ofRuEanOcJ+FSxuwN9HSEOF45tVJnJW5CHMg=
-----END CERTIFICATE-----
" >> /usr/share/pki/ca-trust-source/server.crt
