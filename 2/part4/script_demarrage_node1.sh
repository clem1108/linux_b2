#!/bin/bash
# Clément DOURNET
# 30/09/2020
# script demarrage node 1

# configuration de base

echo "192.168.2.22 node2.tp2.b2" >> /etc/hosts
sudo useradd admin
sudo usermod -aG wheel admin
echo -e "admin\nadmin\n" | sudo passwd admin

# configuration serveur web
sudo useradd web
sudo mkdir /srv/site1
sudo mkdir /srv/site2
sudo touch /srv/site1/index.html
echo "COUCOU !! FROM SITE1" > /srv/site1/index.html
sudo touch /srv/site2/index.html
echo -e "COUCOU !! FROM SITE2" > /srv/site2/index.html
sudo rm /etc/nginx/nginx.conf
sudo touch /etc/nginx/nginx.conf
# fichier de configuration préparé préalablement
echo "#   * Official Russian Documentation: http://nginx.org/ru/docs/

worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;
events {
    worker_connections 1024;
}

http {
        server {
                listen 80;
                server_name node1.tp2.b2;

                location / {
                        return 301 /site1;
                }

                location /site1 {
                        alias /srv/site1/index.html;
                }

                location /site2 {
                        alias /srv/site2/index.html;
                }
        }
        server {
                listen 443 ssl;
                ssl_certificate /etc/pki/tls/certs/server.crt;
                ssl_certificate_key /etc/pki/tls/private/server.key;
                server_name node2.tp1.b2;

                location / {
                        return 301 /site1;
                }

                location /site1 {
                        alias /srv/site1/index.html;
                }

                location /site2 {
                        alias /srv/site2/index.html;
                }
        }
} " >> /etc/nginx/nginx.conf

# certificat généré préalablement
sudo touch /etc/pki/tls/certs/server.crt
sudo touch /etc/pki/tls/private/server.key
sudo chmod 400 /etc/pki/tls/certs/server.crt && sudo chmod 400 /etc/pki/tls/certs/server.crt
sudo echo "-----BEGIN CERTIFICATE-----
MIIDqTCCApGgAwIBAgIJAOnb70Ps9tYwMA0GCSqGSIb3DQEBCwUAMGsxCzAJBgNV
BAYTAkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDEN
MAsGA1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRw
Mi5iMjAeFw0yMDEwMDQxMjUzMjFaFw0yMTEwMDQxMjUzMjFaMGsxCzAJBgNVBAYT
AkZSMRIwEAYDVQQIDAlBUVVJVEFJTkUxEjAQBgNVBAcMCVxCT1JERUFVWDENMAsG
A1UECgwEWU5PVjEOMAwGA1UECwwFTElOVVgxFTATBgNVBAMMDG5vZGUxLnRwMi5i
MjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMi6v6vXOlLAwt7pvb9w
WdCD2AeBHgwFlIURrU13zdfmlDsTUS5dEqgIVxeKnEsoSGwyMLawcZ53kzvE6lxO
O5J6uFXKeq+Wk7NSFtHE71Gf5yWy0ZAyHJ4w+M5jn2EtzuOs4Z39lqjSWTFnvUtD
JigxuQvqRT1NWj844pLhZ5FoK1N96jfA/nFMRwpGf8TepfozoGuoll2cbyPmzrmM
iS6PR9IQRJ7VTjQDtjXjBCbaphCzn6azllsF1a/gi+NbDnkloHcMQ/np3odvlXDw
dlF+t7RPT4z0qEzswKN8mpW5mLtnU+lCEeHJLv4GMBFnhohoFeshv15FL7biFy5Y
vN0CAwEAAaNQME4wHQYDVR0OBBYEFF14NHhFnhi9aEK2C9rUyidj0D6bMB8GA1Ud
IwQYMBaAFF14NHhFnhi9aEK2C9rUyidj0D6bMAwGA1UdEwQFMAMBAf8wDQYJKoZI
hvcNAQELBQADggEBALsmo/oj9aXv5FxJotJNaycLnlDxiCtaPS66gS1KokYaKxMZ
dGT6gyGWJTDf2abuD3Bo1AGt2RnauboFMFILGDt6flhCSpipLmFZC0aAjfDSJfzd
ZEXmgHKmoqEQLKx0gglGCgZIAhWo3Hm67VItXn31lwe1UO4yNsHl/eiHQDuph1aq
ZI/XwxQH26FshrEerzJRAvGityuplOWOXmy9bkdOD2c2Pdz2ix28bOxUIAhWC5HM
9SBCRhgtaXM0wQdMeJbnwvW4ToWcsP4SD/Ff4esX1ZLsW//tro34v4N5yL1LSU6X
fR6ofRuEanOcJ+FSxuwN9HSEOF45tVJnJW5CHMg=
-----END CERTIFICATE-----
" >> /etc/pki/tls/certs/server.crt
echo "-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDIur+r1zpSwMLe
6b2/cFnQg9gHgR4MBZSFEa1Nd83X5pQ7E1EuXRKoCFcXipxLKEhsMjC2sHGed5M7
xOpcTjuSerhVynqvlpOzUhbRxO9Rn+clstGQMhyeMPjOY59hLc7jrOGd/Zao0lkx
Z71LQyYoMbkL6kU9TVo/OOKS4WeRaCtTfeo3wP5xTEcKRn/E3qX6M6BrqJZdnG8j
5s65jIkuj0fSEESe1U40A7Y14wQm2qYQs5+ms5ZbBdWv4IvjWw55JaB3DEP56d6H
b5Vw8HZRfre0T0+M9KhM7MCjfJqVuZi7Z1PpQhHhyS7+BjARZ4aIaBXrIb9eRS+2
4hcuWLzdAgMBAAECggEBAJyTV4nClGMSq69YIsPe7qBBB9UquXZlL+nw1P81saJ0
4TGHqu6AeVoLT2w0mh/iJ0QGjosi7S3QVk/sFWe1OrDS+1EoZTTsPwBEyzoL5SYF
ljeI2gEhpcMbrijH+v3Bzr04ELG9qrS7iAmKw1Se53e4+YA62JGt8fSY+dva6X+4
n5BzI0kTihmgRc9kW/Gu73eh7y0SZRnvk0d5z51/v9kxeD1Np4Mj56MfLF1Ny+Qg
bHxTCsRXPtFLTvGjkJnsPGMoazf+QU1dFmELWUKl0JuqkXuPidBmCb23GHug9aIS
F9DbKNUylu+ZRCi4cpXC0Q4ITnyoPxVN2s3tVwm5m70CgYEA+oBVbkAXo1s9A1m0
XGvo6J8hAt5equqrLFeYT2W8wPgwRXO9cM/i+Ez4HAFVd49ytFaAzlylf6M5dfib
SjZFocWocqHYBNyRP7xeK8Ojk2rWaTK3XcZXJRWpCwcQJazG4FG7s+HwFjqCLEXU
srm0swc5dkXvxoTgTBFyGg1KXdMCgYEAzSK6NyZx0bkwgOaeT2e/k0m6nFCh3puG
01xiLBo4kShgwTlcrvpC6Tb1pqwRB/UIryfnt1jq+9lLXReU42IYAxYRKl2MDjiZ
7WYbzWQNTAQKuk/aB49M+Pvb0CiK6tjGSEj3Dtzh9aIZL6YeDZgXRF/hAZoDqCq9
a5jbeYz/3I8CgYByWPqmZSNlMlTbnqsyNSAQb/NFt/RG2PBiQWoJdEuBWsnOKlMj
mAHnjlsDjahR921I3OPXT3tMlFm6fE2GxtdqTSvjh/Vv4q97BKWIHLoipPZS9w+1
AsDs8MJFgmfgisoNw8GHD2/k9bhONWIuY48U2gtk7e/SEv3AO4Z2VfLIeQKBgE/Y
VY0D3MeaAoQZv8L+ifXle8o+JFNGTBtW2St7ZUsy4T40epvs80BdUQ536QUk3iV3
Ld0SshwnPwtUE3LSCrCg1TudP9eFNnzOAsJWUEqnRBmy3RWWNIqxVaTXMwMoLz2R
kQBq/1ShrJX8XbRC38OQYj9CWGwLfsEztTHBrRGHAoGAUdFQN9fU4RAQngWsUR6z
aFwX/4Kv+xupZIR/yTBxXDix3sN1hE048SkRPmxEItwtDMjmt5W/z/3nptugGUR+
Tbf/VcjsS/Daz4+XUTVn8S1W8LoN68MBckdL2VSqdBzkNcjsCQlg1yHK4IIlBwTJ
MUNOLj+UbnbEJFtcOkmeHDc=
-----END PRIVATE KEY-----
" >> /etc/pki/tls/private/server.key
sudo chown web:web /srv/site*
sudo chown web:web /srv/site1/*
sudo chown web:web /srv/site2/*
sudo chmod 744 /srv/site*
sudo firewall-cmd --add-port=80/tcp --permanent && sudo firewall-cmd --add-port=443/tcp --permanent
sudo firewall-cmd --reload 
sudo systemctl start nginx

# Sauvegarde

sudo touch /opt/tp2_backup.sh
sudo useradd backup
sudo usermod -a -G web backup
sudo mkdir /opt/backup
sudo chown backup:backup /opt/backup && sudo chown backup:backup /opt/tp2_backup.sh
sudo echo "
#!/bin/bash
# Clément DOURNET
# 02/10/2020
# script sauvegarde tp2 
heure=\$(date +%H%M);
jour=\$(date +%Y%m%d);
parametre=\$1;
nom_site=\${parametre##*/};
nom_fichier=\$nom_site'_'\$jour'_'\$heure;
chemin_sauvegarde = '/opt/backup/

if test -z \${parametre};
then
        echo \"Aucun argument n'a été renseigné.\" >&2
        exit 1;
fi

if test ! -d \${chemin_sauvegarde};
then
        echo \"Le chemin de sauvegarde \${chemin_sauvegarde} n'existe pas.\" >&2
        exit 1;
fi

if test ! -d \${parametre};
then
        echo \"Le chemin du site \${parametre} n'existe pas.\" >&2;
        exit 1;
fi


if test -f /opt/backup/lock_\$nom_site;
then
        echo 'Script déja en cours d exécution !';
        exit 22;
fi


touch /opt/backup/lock_\$nom_site;

tar Pzcvf /opt/backup/\$nom_fichier.tar.gz \$parametre --ignore-failed-read > /dev/null

nombre_sauvegarde=\$(ls \${chemin_sauvegarde} | grep -c \${nom_site});

if test \${nombre_sauvegarde} -ge 9;
then
        suppresion_sauvegarde=\$(ls \${chemin_sauvegarde} | grep \${nom_site} | sort | head -n 2 | tail -n 1);
        rm -rf \${chemin_sauvegarde}\${suppression_sauvegarde};
fi

rm /opt/backup/lock_\$nom_site;

" >> /opt/tp2_backup.sh
sudo echo "
0  *  *  *  * backup     bash /opt/tp2_backup.sh /srv/site1
0  *  *  *  * backup     bash /opt/tp2_backup.sh /srv/site2
" >> /etc/crontab


# Netdata

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;


sudo echo "
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/761275561540190228/CQuKu4Y1-rCbeQrl-7DCkuf5nFCcbJS2FMulTZjOMwbbQ_cbj4tNxmDh05lG2F6PkjqZ"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
" >> /etc/netdata/health_alarm_notify.conf
