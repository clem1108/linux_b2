#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node2 mariadb

# install mariadb

yum install -y mariadb-server

systemctl enable mariadb

systemctl start mariadb

echo "#bind-address = 192.168.4.42" | tee -a /etc/my.cnf

firewall-cmd --add-port=3306/tcp --permanent
firewall-cmd --reload

echo -e "CREATE USER 'gitea'@'192.168.4.41' IDENTIFIED BY 'gitea';\nCREATE DATABASE giteadb CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';\nGRANT ALL PRIVILEGES ON giteadb.* TO 'gitea'@'192.168.4.41';\nFLUSH PRIVILEGES;\nexit\n " | mysql -u root

systemctl restart mariadb


