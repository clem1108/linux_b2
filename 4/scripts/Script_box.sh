#!/bin/bash
# Clément DOURNET
# 13.10.2020
# Scrip creation box tp4

yum update -y

yum install vim -y

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;

echo "
192.168.4.41 node1.tp4.b2
192.168.4.42 node2.tp4.b2
192.168.4.43 node3.tp4.b2
192.168.4.44 node4.tp4.b2" >> /etc/hosts

setenforce 0

echo "

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted" > /etc/selinux/config

systemctl enable firewalld

systemctl start firewalld

firewall-cmd --permanent --new-zone="custom"


echo "<?xml version='1.0' encoding='utf-8'?>
<zone target='DROP'>
<short>Custom Zone Configuration</short>
<description>All incomming connections are blocked by default. Only specific services are allowed.</description>
<service name='ssh'/>
<port protocol=\"tcp\" port=\"19999\" />
</zone>" > /etc/firewalld/zones/custom.xml

firewall-cmd --reload

firewall-cmd --set-default-zone="custom"

firewall-cmd --reload

echo "
url='https://discordapp.com/api/webhooks/765496270755201034/UvIgqR-bZSalH1pS-E_bbeD_hBcEB9pX86Adg3z2huNoJ-NX2X0Ij6EDvJ0eHsGWCecy'
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  \"CHANNEL1 CHANNEL2 ...\"

# enable/disable sending discord notifications
SEND_DISCORD=\"YES\"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL=\"${url}\"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD=\"alarms\"
" >> /etc/netdata/health_alarm_notify.conf
