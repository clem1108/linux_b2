#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node1 gitea

# install gitea

yum install -y wget 

wget -O gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64
chmod +x gitea

adduser --system --shell /bin/bash --home /home/git git
echo "git     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

mkdir -p /var/lib/gitea/{custom,data,log}
chown -R git:git /var/lib/gitea/
chmod -R 750 /var/lib/gitea/
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea

cp gitea /usr/local/bin/gitea

echo "[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target

#Requires=mariadb.service

[Service]
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
ExecStart=/usr/bin/sudo /usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea

[Install]
WantedBy=multi-user.target" | tee /etc/systemd/system/gitea.service

echo "APP_NAME = Gitea: Git with a cup of tea
RUN_USER = root
RUN_MODE = prod

[oauth2]
JWT_SECRET = MeHFPQByKvX7pJlUoBRS0C4ex8dGQbTlV5DZ-i5lNjM

[security]
INTERNAL_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE2MDM1NDA4NTh9.xllLsO5yPctGKV-yNwb-ccoaeaWcwsgKhxj6Bv9HCZE
INSTALL_LOCK   = true
SECRET_KEY     = jPfbY1rLhB7aejcM5KO9rkPnjIZeVSWQEnGZFHW70eiCPnAuxUnpg3FYGTVyuKzK

[database]
DB_TYPE  = mysql
HOST     = 192.168.4.42:3306
NAME     = giteadb
USER     = gitea
PASSWD   = gitea
SCHEMA   =
SSL_MODE = disable
CHARSET  = utf8
PATH     = /usr/local/bin/data/gitea.db

[repository]
ROOT = /root/gitea-repositories

[server]
SSH_DOMAIN       = 192.168.4.41
DOMAIN           = tp4.b2
HTTP_PORT        = 3000
ROOT_URL         = http://192.168.4.41:3000/
DISABLE_SSH      = false
SSH_PORT         = 22
LFS_START_SERVER = true
LFS_CONTENT_PATH = /usr/local/bin/data/lfs
LFS_JWT_SECRET   = R0fyFTLAe5-6BSA5m8CdWQIrqbco6Najfx7_qXdSQsU
OFFLINE_MODE     = false

[mailer]
ENABLED = false

[service]
REGISTER_EMAIL_CONFIRM            = false
ENABLE_NOTIFY_MAIL                = false
DISABLE_REGISTRATION              = false
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = false
REQUIRE_SIGNIN_VIEW               = false
DEFAULT_KEEP_EMAIL_PRIVATE        = false
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply.localhost

[picture]
DISABLE_GRAVATAR        = false
ENABLE_FEDERATED_AVATAR = true

[openid]
ENABLE_OPENID_SIGNIN = true
ENABLE_OPENID_SIGNUP = true

[session]
PROVIDER = file

[log]
MODE      = file
LEVEL     = info
ROOT_PATH = /usr/local/bin/log
" > /etc/gitea/app.ini

firewall-cmd --add-port=3000/tcp --permanent

firewall-cmd --reload

systemctl daemon-reload

systemctl start gitea

# sauvegarde

mkdir -p /media/nfs

mount 192.168.4.44:/opt/gitea /media/nfs

echo "#!/bin/bash
# Clément Dournet
# 23/10/2020
# script sauvegarde gitea + db

jour=\$(date +%Y%m%d);

if test -f /media/nfs/lock_\$jour;
then
        echo 'Script déja en cours d exécution !';
        exit 22;
fi

sudo -u nfsnobody touch /media/nfs/lock_\$jour;
/usr/local/bin/gitea dump -c /etc/gitea/app.ini

cp gitea-dump-* /media/nfs

rm -rf gitea-dump-*

sudo -u nfsnobody rm /media/nfs/lock_\$jour;" > /home/vagrant/Script_gitea.sh

chmod 700 /home/vagrant/Script_gitea.sh

echo "  0  0  *  *  * root bash /home/vagrant/Script_gitea.sh" | sudo tee -a /etc/crontab
