#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node3 nginx

# install nginx

yum install -y epel-release
yum install -y nginx

echo "#   * Official Russian Documentation: http://nginx.org/ru/docs/

worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
events {
    worker_connections 1024;
}

http {
        server {
    listen 80;
    server_name gitea.tp4.b2;

    location / {
        proxy_pass http://192.168.4.41:3000;
    }
}
}" > /etc/nginx/nginx.conf

firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --reload

systemctl enable nginx
systemctl start nginx

# sauvegarde

mkdir -p /media/nfs

mount 192.168.4.44:/opt/nginx /media/nfs

echo "#!/bin/bash
# Clément Dournet
# 25/10/2020
# Script Sauvegarde nginx

jour=\$(date +%Y%m%d);

if test -f /media/nfs/lock_\$jour;
then
        echo 'Script déjà en cours d exécution !';
	exit 22;
fi

sudo -u nfsnobody touch /media/nfs/lock_\$jour;

sudo -u nfsnobody cp /etc/nginx/nginx.conf /media/nfs/nginx.conf

sudo -u nfsnobody rm -rf /media/nfs/lock_\$jour" > /home/vagrant/Script_nginx.sh

chmod 700 /home/vagrant/Script_nginx.sh

echo "  0  0  *  *  * root bash /home/vagrant/Script_nginx.sh" | sudo tee -a /etc/crontab

