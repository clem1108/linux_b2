#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node4 serveur nfs

# install nfs

mkdir /opt/gitea
mkdir /opt/nginx

firewall-cmd --permanent --add-service=nfs
firewall-cmd --reload

yum install -y nfs-utils

echo "[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp4.b2" > /etc/idmapd.conf

echo "/opt/gitea  192.168.4.0/24(rw,async) \
	         *.tp4.b2(ro,async,no_subtree_check)
/opt/nginx  192.168.4.0/24(rw,async) \
                  *.tp4.b2(ro,async,no_subtree_check)" >> /etc/exports

systemctl enable rpcbind nfs-server --now

chown nfsnobody:nfsnobody /opt/*

chmod 755 /opt/*
