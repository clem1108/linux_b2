# TP4 : Déploiement multi-noeud

## 0. Prerequisites

J'ai décidé de créer une box repackagé à l'aide de ce script : 

```bash
#!/bin/bash
# Clément DOURNET
# 13.10.2020
# Scrip creation box tp4

# Mise à jour de l'OS

yum update -y

# installation de VIM

yum install vim -y

# installation de Netdata

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait;

# remplissage du fichier /etc/hosts

echo "
192.168.4.41 node1.tp4.b2
192.168.4.42 node2.tp4.b2
192.168.4.43 node3.tp4.b2
192.168.4.44 node4.tp4.b2" >> /etc/hosts

# Désactivation de SELinux

setenforce 0

echo "

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted" > /etc/selinux/config

# Configuration du firewall

systemctl enable firewalld

systemctl start firewalld

firewall-cmd --permanent --new-zone="custom"


echo "<?xml version='1.0' encoding='utf-8'?>
<zone target='DROP'>
<short>Custom Zone Configuration</short>
<description>All incomming connections are blocked by default. Only specific services are allowed.</description>
<service name='ssh'/>
<port protocol=\"tcp\" port=\"19999\" />
</zone>" > /etc/firewalld/zones/custom.xml

firewall-cmd --reload

firewall-cmd --set-default-zone="custom"

firewall-cmd --reload

# Notification netdata dans Discord

echo "
url='https://discordapp.com/api/webhooks/765496270755201034/UvIgqR-bZSalH1pS-E_bbeD_hBcEB9pX86Adg3z2huNoJ-NX2X0Ij6EDvJ0eHsGWCecy'
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  \"CHANNEL1 CHANNEL2 ...\"

# enable/disable sending discord notifications
SEND_DISCORD=\"YES\"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL=\"${url}\"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD=\"alarms\"
" >> /etc/netdata/health_alarm_notify.conf

``` 

<a href="scripts/Script_box.sh">Script de création de la box </a>

Afin de pouvoir recevoir des notifications dans un channel discord personnalisé, il suffit juste de remplacer le contenu de la variable `$url` en haut di fichier par la nouvelle URL.

Afin de créer la box, il est nécessaire de créer une VM qui démarrera en lançant le script. Pour cela, on crée un `Vagrantfile` dédié que voici : 

```ruby
Vagrant.configure("2") do |config|
  # Configuration commune à toutes les machines
    config.vm.box = "centos/7"
    config.vbguest.auto_update = false
    config.vm.box_check_update = false 
    config.vm.synced_folder ".", "/vagrant", disabled: true

# Config une première VM "node1"
    config.vm.define "node1" do |node1|
  # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
        node1.vm.network "private_network", ip: "192.168.4.41"
	node1.vm.hostname = "node1.tp4.b2"
	node1.vm.provision "shell", path: "Script_box.sh"
        node1.vm.provider "virtualbox" do |v1|
	    v1.name = "node1"
            v1.memory = 1024
	    end
     end
end
```

<a href="Vagrant/Vagrantfile_creation_box">VagrantFile pour la création de la box</a>

Après avoir lançé la VM et que l'exécution du script est terminée, on exécute ces deux commandes : 

```shell
PS C:\Users\cleme\vagrant> vagrant package --output tp4_centos7.box
==> node1: Attempting graceful shutdown of VM...
==> node1: Clearing any previously set forwarded ports...
==> node1 Exporting VM...
==> node1: Compressing package to: C:/Users/cleme/vagrant/tp4_centos7.box
PS C:\Users\Crenn Antoine\Desktop\Cours\B2\Linux\vagrant> vagrant box add tp4_centos7 tp4_centos7.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'tp4_centos7' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/cleme/vagrant/tp4_centos7.box
    box:
==> box: Successfully added box 'tp4_centos7' (v0) for 'virtualbox'!
```

La box repackagée peut donc être utilisée.

## I. Consignes générales

Voici le tableau indiquant les infos des différentes machines.

| Name               | IP   | Rôle       |
|--------------------|------|------------|
| `node1.tp4.b2` | <192.168.4.41> | Gitea |
| `node2.tp4.b2`  | <192.168.4.42> | Base de données |
| `node3.tp4.b2`  | <192.168.4.43> | NGINX |
| `node4.tp4.b2`  | <192.168.4.44> | NFS |

Pour ce TP, le `Vagrantfile` utilisé sera celui-ci : 

```ruby
Vagrant.configure("2") do |config|
  # Configuration commune à toutes les machines
    config.vm.box = "tp4_centos7.box"
    config.vbguest.auto_update = false
    config.vm.box_check_update = false 
    config.vm.synced_folder ".", "/vagrant", disabled: true

# Config une première VM "node4"
config.vm.define "node4" do |node4|
# remarquez l'utilisation de 'node4.' défini sur la ligne au dessus
 node4.vm.network "private_network", ip: "192.168.4.44"
 node4.vm.hostname = "node4.tp4.b2"
 node4.vm.provision "shell", path: "Script_node4.sh"
 node4.vm.provider "virtualbox" do |v4|
                  v4.name = "node4"
		  v4.memory = 512
		  end
	end  

# Config une première VM "node2"
    config.vm.define "node2" do |node2|
  # remarquez l'utilisation de 'node2.' défini sur la ligne au dessus
        node2.vm.network "private_network", ip: "192.168.4.42"
        node2.vm.hostname = "node2.tp4.b2"
        node2.vm.provision "shell", path: "Script_node2.sh"
        node2.vm.provider "virtualbox" do |v2|
            v2.name = "node2"
            v2.memory = 512
            end
     end 

# Config une première VM "node1"
    config.vm.define "node1" do |node1|
  # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
        node1.vm.network "private_network", ip: "192.168.4.41"
	node1.vm.hostname = "node1.tp4.b2"
	node1.vm.provision "shell", path: "Script_node1.sh"
        node1.vm.provider "virtualbox" do |v1|
	    v1.name = "node1"
            v1.memory = 1024
	    end
     end

# Config une première VM "node3"
     config.vm.define "node3" do |node3|
# remarquez l'utilisation de 'node3.' défini sur la ligne au dessus
           node3.vm.network "private_network", ip: "192.168.4.43"
           node3.vm.hostname = "node3.tp4.b2"
           node3.vm.provision "shell", path: "Script_node3.sh"
           node3.vm.provider "virtualbox" do |v3|
                v3.name = "node3"
                v3.memory = 512
		end
	end
end
```

<a href="Vagrantfile">VagrantFile</a>

La première machine à se lancer est le serveur `NFS` (node4), le second le serveur `MariaDB` (node2), le troisième `Gitea` (node1) et enfin le serveur `reverse proxy` (node3).

## II. Présentation de la stack

### 1. Gitea

Afin de lancer ce service, je crée un script d'installation au démarrage que voici :

```bash
#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node1 gitea

# install gitea

yum install -y wget 

wget -O gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64
chmod +x gitea

# Création d'un user

adduser --system --shell /bin/bash --home /home/git git
echo "git     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

# Création des différents dossiers pour gitea

mkdir -p /var/lib/gitea/{custom,data,log}
chown -R git:git /var/lib/gitea/
chmod -R 750 /var/lib/gitea/
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea

cp gitea /usr/local/bin/gitea

# Création de l'unité

echo "[Unit]
Description=Gitea (Git with a cup of tea)
After=syslog.target
After=network.target

#Requires=mariadb.service

[Service]
RestartSec=2s
Type=simple
User=git
Group=git
WorkingDirectory=/var/lib/gitea/
ExecStart=/usr/bin/sudo /usr/local/bin/gitea web --config /etc/gitea/app.ini
Restart=always
Environment=USER=git HOME=/home/git GITEA_WORK_DIR=/var/lib/gitea

[Install]
WantedBy=multi-user.target" | tee /etc/systemd/system/gitea.service

# Mise en place du service

echo "APP_NAME = Gitea: Git with a cup of tea
RUN_USER = root
RUN_MODE = prod

[oauth2]
JWT_SECRET = MeHFPQByKvX7pJlUoBRS0C4ex8dGQbTlV5DZ-i5lNjM

[security]
INTERNAL_TOKEN = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE2MDM1NDA4NTh9.xllLsO5yPctGKV-yNwb-ccoaeaWcwsgKhxj6Bv9HCZE
INSTALL_LOCK   = true
SECRET_KEY     = jPfbY1rLhB7aejcM5KO9rkPnjIZeVSWQEnGZFHW70eiCPnAuxUnpg3FYGTVyuKzK

[database]
DB_TYPE  = mysql
HOST     = 192.168.4.42:3306
NAME     = giteadb
USER     = gitea
PASSWD   = gitea
SCHEMA   =
SSL_MODE = disable
CHARSET  = utf8
PATH     = /usr/local/bin/data/gitea.db

[repository]
ROOT = /root/gitea-repositories

[server]
SSH_DOMAIN       = 192.168.4.41
DOMAIN           = tp4.b2
HTTP_PORT        = 3000
ROOT_URL         = http://192.168.4.41:3000/
DISABLE_SSH      = false
SSH_PORT         = 22
LFS_START_SERVER = true
LFS_CONTENT_PATH = /usr/local/bin/data/lfs
LFS_JWT_SECRET   = R0fyFTLAe5-6BSA5m8CdWQIrqbco6Najfx7_qXdSQsU
OFFLINE_MODE     = false

[mailer]
ENABLED = false

[service]
REGISTER_EMAIL_CONFIRM            = false
ENABLE_NOTIFY_MAIL                = false
DISABLE_REGISTRATION              = false
ALLOW_ONLY_EXTERNAL_REGISTRATION  = false
ENABLE_CAPTCHA                    = false
REQUIRE_SIGNIN_VIEW               = false
DEFAULT_KEEP_EMAIL_PRIVATE        = false
DEFAULT_ALLOW_CREATE_ORGANIZATION = true
DEFAULT_ENABLE_TIMETRACKING       = true
NO_REPLY_ADDRESS                  = noreply.localhost

[picture]
DISABLE_GRAVATAR        = false
ENABLE_FEDERATED_AVATAR = true

[openid]
ENABLE_OPENID_SIGNIN = true
ENABLE_OPENID_SIGNUP = true

[session]
PROVIDER = file

[log]
MODE      = file
LEVEL     = info
ROOT_PATH = /usr/local/bin/log
" > /etc/gitea/app.ini

# ouverture du port et lancement du service 

firewall-cmd --add-port=3000/tcp --permanent

firewall-cmd --reload

systemctl daemon-reload

systemctl start gitea

# script sauvegarde + automatisation

mkdir -p /media/nfs

mount 192.168.4.44:/opt/gitea /media/nfs

echo "#!/bin/bash
# Clément Dournet
# 23/10/2020
# script sauvegarde gitea + db

jour=\$(date +%Y%m%d);

if test -f /media/nfs/lock_\$jour;
then
        echo 'Script déja en cours d exécution !';
        exit 22;
fi

sudo -u nfsnobody touch /media/nfs/lock_\$jour;
/usr/local/bin/gitea dump -c /etc/gitea/app.ini

cp gitea-dump-* /media/nfs

rm -rf gitea-dump-*

sudo -u nfsnobody rm /media/nfs/lock_\$jour;" > /home/vagrant/Script_gitea.sh

chmod 700 /home/vagrant/Script_gitea.sh

echo "  0  0  *  *  * root bash /home/vagrant/Script_gitea.sh" | sudo tee -a /etc/crontab
```

<a href="scripts/Script_node1.sh">Script Gitea</a>

Ce service est maintenant totalement configuré et fonctionnel.

### 2. MariaDB

Pour ce service, j'ai créé ce script : 

```bash
#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node2 mariadb

# install mariadb

yum install -y mariadb-server

systemctl enable mariadb

systemctl start mariadb

# Configuration du service et ouverture du port

echo "#bind-address = 192.168.4.42" | tee -a /etc/my.cnf

firewall-cmd --add-port=3306/tcp --permanent
firewall-cmd --reload

echo -e "CREATE USER 'gitea'@'192.168.4.41' IDENTIFIED BY 'gitea';\nCREATE DATABASE giteadb CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';\nGRANT ALL PRIVILEGES ON giteadb.* TO 'gitea'@'192.168.4.41';\nFLUSH PRIVILEGES;\nexit\n " | mysql -u root

systemctl restart mariadb
```
<a href="scripts/Script_node2.sh">Script MariaDB</a>

Ce service sert à la sauvegarde des infos des utilisateurs de `Gitea`.

### 3. NGINX

Ici aussi, j'ai utilisé un script qui s'exécute au démarrage :

```bash
#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node3 nginx

# install nginx

yum install -y epel-release
yum install -y nginx

# Configuration du service

echo "#   * Official Russian Documentation: http://nginx.org/ru/docs/

worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
events {
    worker_connections 1024;
}

http {
        server {
    listen 80;
    server_name gitea.tp4.b2;

    location / {
        proxy_pass http://192.168.4.41:3000;
    }
}
}" > /etc/nginx/nginx.conf

# ouverture du port et démarrage du service

firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --reload

systemctl enable nginx
systemctl start nginx

# sauvegarde

mkdir -p /media/nfs

mount 192.168.4.44:/opt/nginx /media/nfs

echo "#!/bin/bash
# Clément Dournet
# 25/10/2020
# Script Sauvegarde nginx

jour=\$(date +%Y%m%d);

if test -f /media/nfs/lock_\$jour;
then
        echo 'Script déjà en cours d exécution !';
	exit 22;
fi

sudo -u nfsnobody touch /media/nfs/lock_\$jour;

sudo -u nfsnobody cp /etc/nginx/nginx.conf /media/nfs/nginx.conf

sudo -u nfsnobody rm -rf /media/nfs/lock_\$jour" > /home/vagrant/Script_nginx.sh

chmod 700 /home/vagrant/Script_nginx.sh

echo "  0  0  *  *  * root bash /home/vagrant/Script_nginx.sh" | sudo tee -a /etc/crontab
```

<a href="scripts/Script_node3.sh">Script NGINX</a>

Une fois ce service lancé, `Gitea` est totalement fonctionnel et complet !

### 4. Serveur NFS et sauvegarde 

Afin de pouvoir rendre la sauvegarde possible, il est nécessaire de monter un serveur NFS. J'ai donc utilisé ce script :

```bash
#!/bin/bash
# Clément Dournet
# 13.10.2020
# Script demarrage node4 serveur nfs

# install nfs
yum install -y nfs-utils

# Création des dossiers à partager

mkdir /opt/gitea
mkdir /opt/nginx

# Ouverture des ports

firewall-cmd --permanent --add-service=nfs
firewall-cmd --reload

# Mise en place du partage

echo "[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp4.b2" > /etc/idmapd.conf

echo "/opt/gitea  192.168.4.0/24(rw,async) \
	         *.tp4.b2(ro,async,no_subtree_check)
/opt/nginx  192.168.4.0/24(rw,async) \
                  *.tp4.b2(ro,async,no_subtree_check)" >> /etc/exports

systemctl enable rpcbind nfs-server --now

chown nfsnobody:nfsnobody /opt/*

chmod 755 /opt/*
```

Une fois le service lancé, la sauvegarde est donc fonctionnelle. La sauvegarde s'exécute donc de manière journalière et les données sont sauvegardées sur le serveur NFS dans un dossier dédié : 

* Dossier `gitea` : comprend la sauvegarde complète du service `Gitea` avec sa base de donnée
* DOssier `nginx` : comprend le fichier de configuration du service `nginx` 

### 5. Monitoring 

Chaque machine est monitorée à l'aide du service `Netdata`. Ce service s'installe dans la box repackagée. Voici un exemple d'alerte : 

<img src="pics/netdata.png"/>

### 6. Conclusion

Afin de pouvoir accéder à `Gitea`, il est nécessaire de taper dans la barre d'adresse l'IP `192.168.4.43`. Cette IP correspond au serveur `NGINX` qui sert de `reverse proxy`, donc redirige vers le serveur `Gitea` à l'adresse `192.168.4.41:3000`. Une fois arrivé sur le site, l'utilisateur se connecte ou s'inscrit. Ses données seront enregistrées dans une base de données se trouvant sur le serveur `MariaDB` à l'adresse `192.168.4.42`.

Pour la savegarde des données, 
* Si l'on souhaite sauvegarder le serveur `Gitea` avec sa base de données, il suffit d'accéder en `SSH` à la machine en tapant `vagrant ssh node1`. Ensuite, il suffit de taper la commande `sudo ./Script_gitea.sh`. La sauvegarde va coper toutes les données correspondant au service `Gitea`, que ce soit la configuration du site, la base de connées, ou encore les différents `repositories`créés. Ces données sont stockées dans une archive qui ensuite copié sur le serveur `NFS` à l'adresse `192.168.4.44` dans le dossier `/opt/gitea`.
* Si l'on souhaite sauvegarder le service `NGINX`, qui sert ici de `reverse proxy`, il est nécessaire de se connecter en `SSH` en tapant la commande `vagrant ssh node3`. Une fois connecté, il faut taper la commande `sudo ./Script_nginx.sh`. La commande va lancer le script qui va copier le fichier de configuration du serveur NFS se trouvant dans `/etc/nginx/nginx.conf`. Ce fichier sera ensuite copié sur le serveur `NFS` à l'adresse `192.168.4.44` dans le dossier `/opt/nginx`.

### 7. Bonus 

#### Vérification du binaire Gitea 

Nous allons vérifier l'intégrité du fichier `gitea` téléchargé auparavant : 

```bash
[vagrant@node1 ~]$ wget -O gitea.sha256 https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64.sha256
--2020-10-25 19:36:52--  https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64.sha256
Resolving dl.gitea.io (dl.gitea.io)... 147.75.84.81, 2604:1380:2000:c600::5
Connecting to dl.gitea.io (dl.gitea.io)|147.75.84.81|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 91 [application/octet-stream]
Saving to: ‘gitea.sha256’

100%[==============================================================================>] 91          --.-K/s   in 0s

2020-10-25 19:36:53 (36.0 MB/s) - ‘gitea.sha256’ saved [91/91]

[vagrant@node1 ~]$ cat gitea.sha256
8ed8bff1f34d8012cab92943214701c10764ffaca102e311a3297edbb8fce940  gitea-1.12.5-linux-amd64
[vagrant@node1 ~]$ sha256sum gitea
8ed8bff1f34d8012cab92943214701c10764ffaca102e311a3297edbb8fce940  gitea
``` 

Les fichiers n'ont donc pas de problème d'intégrité puisque l'empreinte de référence est identique à celle comprise dans le paquet `gitea`.


<img src=pics/end.gif/>
