# B2 TP1 : Déploiement classique
## 0. Prérequis
### node 1
Pour modifier les fichiers de configuration, il est nécessaire de taper la commande `sudo vim [emplacement et nom du fichier]`.
* Partitionnement

Regardons les disques installés : 
```bash
[clement@node1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6.2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0    5G  0 disk
sr0              11:0    1 1024M  0 rom
```
Nous pouvons voir qu'il y a un disque libre `sdb`.
Créons d'abord le `Logical Volume` : 
```bash
[clement@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for clement:
  Physical volume "/dev/sdb" successfully created.
 ```
 Vérification : 
 ```bash 
 [clement@node1 ~]$ sudo pvs
  PV         VG     Fmt  Attr PSize  PFree
  /dev/sda2  centos lvm2 a--  <7.00g    0
  /dev/sdb          lvm2 ---   5.00g 5.00g
 ```
 Créons ensuite le `Volume Group` : 
 ```bash
 [clement@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
 ```
 Vérification : 
 ```bash
 [clement@node1 ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  centos   1   2   0 wz--n- <7.00g    0
  data     1   2   0 wz--n- <5.00g    0
 ```
 Créons ensuite les `Logical Volumes` : 
 ```bash
 [clement@node1 ~]$ sudo lvcreate -L 2G data -n data1
  Logical volume "data1" created.
 [clement@node1 ~]$ sudo lvcreate -l 100%FREE data -n data2
  Logical volume "data2" created
 ```
 Vérification : 
 ```bash
 [clement@node1 ~]$ sudo lvs
  LV    VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root  centos -wi-ao----  <6.20g

  swap  centos -wi-ao---- 820.00m

  data1 data   -wi-a-----   2.00g

  data2 data   -wi-a-----  <3.00g
 ```
 Créons le système de fichiers sur les deux partitions : 
 ```bash
 [clement@node1 ~]$ sudo mkfs -t ext4 /dev/data/data1
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
131072 inodes, 524288 blocks
26214 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done

[clement@node1 ~]$ sudo mkfs -t ext4 /dev/data/data2
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
196608 inodes, 785408 blocks
39270 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=805306368
24 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

Configurons le montage automatique des deux partitions : 
Il faut d'abord créer les deux dossiers : 
```bash
[clement@node1 ~]$ sudo mkdir /srv/data1
[clement@node1 ~]$ sudo mkdir /srv/data2
```
Nous pouvons maintenant planifier le montage automatique : 
```bash
[clement@node1 ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 23 09:39:06 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=759b8fe5-ac65-4031-80ef-f8b76b726643 /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0

/dev/data/data1 /srv/site1 ext4 defaults 0 0
/dev/data/data2 /srv/site2 ext4 defaults 0 0
```
Vérification : 
```bash 
[clement@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
swap                     : ignored
mount: /srv/site1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/site1               : successfully mounted
mount: /srv/site2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/srv/site2               : successfully mounted
```
Vérification : 
```bash
[clement@node2 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6.2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0    5G  0 disk
├─data-data1    253:2    0    2G  0 lvm  /srv/site1
└─data-data2    253:3    0    3G  0 lvm  /srv/site2
sr0              11:0    1 1024M  0 rom
```
Le montage automatique est donc fonctionnel.

* Accès internet

Voici le fichier de configuration de la carte NAT :
```bash
[clement@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="dhcp"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="enp0s3"
UUID="9d672249-1a25-41e7-b990-cb90bd9c762f"
DEVICE="enp0s3"
ONBOOT="yes"
```
Nous pouvons voir que la configuration est en DHCP et qu'elle est configurée pour être la route par défaut.

Maintenant, testons la connectivité à Internet : 
```bash
[clement@node1 ~]$ curl google.com
<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
<TITLE>301 Moved</TITLE></HEAD><BODY>
<H1>301 Moved</H1>
The document has moved
<A HREF="http://www.google.com/">here</A>.
</BODY></HTML>
```
La machine communique donc bien avec internet.
* Accès réseau local 

Voici le fichier de configuration de la carte hosts-only :
```bash
[clement@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=192.168.1.11
MASK=255.255.255.0
```
Essaysons de voir si `node1` arrive à ping `node2` : 
```bash
[clement@node1 ~]$ ping 192.168.1.12 -c 4
PING 192.168.1.12 (192.168.1.12) 56(84) bytes of data.
64 bytes from 192.168.1.12: icmp_seq=1 ttl=64 time=0.229 ms
64 bytes from 192.168.1.12: icmp_seq=2 ttl=64 time=0.442 ms
64 bytes from 192.168.1.12: icmp_seq=3 ttl=64 time=0.342 ms
64 bytes from 192.168.1.12: icmp_seq=4 ttl=64 time=0.390 ms

--- 192.168.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3055ms
rtt min/avg/max/mdev = 0.229/0.350/0.442/0.081 ms
```
Les deux machines communiquent.
 * Nom de la machine

Pour modifier le `hostname`, il faut taper la commande `sudo vim /etc/hostname`, ce qui donne : 
```bash
[clement@node1 ~]$ cat /etc/hostname
node1.tp1.b2
```
* Communication des machines

Il est nécéssaire de modifier le fichier en tapant `sudo vim /etc/hosts`
```bash
[clement@node1 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.12 node2.tp1.b2
```
* Création utilisateur

```bash
[clement@node1 ~]$ sudo useradd -m admin
[sudo] password for clement:
[clement@node1 ~]$ passwd admin
passwd: Only root can specify a user name.
[clement@node1 ~]$ sudo !!
sudo passwd admin
Changing password for user admin.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
```
L'utilisateur est créé avec un `HOMEDIR` et un mot de passe.
```bash
[clement@node1 ~]$ sudo cat /etc/sudoers | grep -e admin -e root
## the root user, without needing the root password.
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL
admin   ALL=(ALL)       ALL
## cdrom as root
```
On vient de lui octroyer le droit de passer des commandes en `sudo` afin qu'il ait les permissions de faire la configuration système et l'accès à certains dossiers protégés.
* SSH

On génére la clé en tapant la commande `ssh-keygen`. Comme j'ai déjà une paire de clés sur ma machine hôte, je n'ai pas besoin d'en générer de nouvelle.
Je copie ensuite ma clé publique dans une fichier nommé `authorized_keys` avec la commande `sudo vim .ssh/authorized_keys`.

* Firewall

Pour cela, j'ai créé une nouvelle zone que j'ai appelé `custom`. Voici le fichier de configuration : 
```bash
[clement@node1 ~]$ sudo cat /etc/firewalld/zones/custom.xml
[sudo] password for clement:
<?xml version="1.0" encoding="utf-8"?>
<zone target="DROP">
  <short>Custom Zone Configuration</short>
  <description>All incomming connections are blocked by default. Only specific services are allowed.</description>
  <service name="ssh"/>
</zone>
```
J'ajoute une nouvelle règle pour autoriser le ping : 
```bash
[clement@node1 ~]$ sudo firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 0 -p icmp -s 0.0.0.0/0 -d 0.0.0.0/0 -j ACCEPT
[clement@node1 ~]$ sudo firewall-cmd --reload
success
```
Vérification
```bash
[admin@node1 ~]$ sudo firewall-cmd --list-all
custom
  target: DROP
  icmp-block-inversion: no
  interfaces:
  sources:
  services: ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
Le firewall rejette donc toutes les connexion et laisse passer les connexions ssh et ping.

* SELINUX
Afin de ne pas être bloqué par les règles de sécurité, il est nécessaire de désactiver `SELINUX`. Pour cela, on tape d'abord la commande `sudo setenforce 0` et je modifie le fichier de configuration afin que la modification soit permanente : 
```bash
[admin@node1 ~]$ cat /etc/selinux/config

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=permissive
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```

### node 2 

La configuration du `node 2` est identique à celle du `node1` excepté ces quelques points : 

* Accès réseau local 

Voici le fichier de configuration de la carte hosts-only :
```bash
[clement@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=192.168.1.12
MASK=255.255.255.0
```
Essaysons de voir si `node2` arrive à ping `node1` : 
```bash
[admin@node2 ~]$ ping 192.168.1.11
PING 192.168.1.11 (192.168.1.11) 56(84) bytes of data.
64 bytes from 192.168.1.11: icmp_seq=1 ttl=64 time=0.478 ms
64 bytes from 192.168.1.11: icmp_seq=2 ttl=64 time=0.378 ms
64 bytes from 192.168.1.11: icmp_seq=3 ttl=64 time=0.374 ms
64 bytes from 192.168.1.11: icmp_seq=4 ttl=64 time=0.363 ms
^C
--- 192.168.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3387ms
rtt min/avg/max/mdev = 0.363/0.398/0.478/0.048 ms
```
Les deux machines communiquent.
 * Nom de la machine

Pour modifier le `hostname`, il faut taper la commande `sudo vim /etc/hostname`, ce qui donne : 
```bash
[clement@node2 ~]$ cat /etc/hostname
node2.tp1.b2
```
* Communication des machines

Il est nécéssaire de modifier le fichier en tapant `sudo vim /etc/hosts`
```bash
[clement@node1 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.11 node1.tp1.b2


## Setup serveur web

Pour installer le serveur web, il faut au préalable taper la commande `sudo yum install -y epel-release` puis `sudo yum install -y nginx`.

Je crée l'utilisateur `web` avec la commande `sudo useradd web`.

Ensuite, je crée les fichiers nécessaires avec les autorisations les plus restrictives : 
```bash
[admin@node1 ~]$ sudo ls -al /srv/site*
/srv/site1:
total 24
drwxrwxr--. 3 web  web   4096 Sep 24 11:37 .
drwxr-xr-x. 4 root root    32 Sep 24 10:25 ..
-rw-r--r--. 1 web  web     24 Sep 24 11:37 index.html
drwx------. 2 web  web  16384 Sep 23 12:01 lost+found

/srv/site2:
total 24
drwxrwxr--. 3 web  web   4096 Sep 24 11:37 .
drwxr-xr-x. 4 root root    32 Sep 24 10:25 ..
-rw-r--r--. 1 web  web     21 Sep 24 11:37 index.html
drwx------. 2 web  web  16384 Sep 23 12:01 lost+found
```

Le dossiers appartiennent donc à l'utilisateur `web`, il a les droits nécessaires à la lecture et modification de ces fichiers, alors que le groupe `web`et les autres utilisateurs ont juste le droit de lecture.

Ensuite, j'autorise les ports `80` et `443` avec la commande `sudo firewal-cmd --add-port=80/tcp --permanent` et `sudo firewal-cmd --add-port=443/tcp --permanent`.

Je vérifie les ports : 
```bash
[admin@node1 ~]$ sudo firewall-cmd --list-all
custom (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 80/tcp 443/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Les ports autorisés sont bien `80` pour l'`HTTP` et `443` pour l'`HTTPS`

Voici le fichier de configuration du serveur web : 
```bash
[admin@node1 ~]$ sudo cat /etc/nginx/nginx.conf

#   * Official Russian Documentation: http://nginx.org/ru/docs/

worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;
events {
    worker_connections 1024;
}

http {
        server {
                listen 80;
                server_name node1.tp1.b2;

                location / {
                        return 301 /site1;
                }

                location /site1 {
                        alias /srv/site1/index.html;
                }

                location /site2 {
                        alias /srv/site2/index.html;
                }
        }
        server {
                listen 443 ssl;
                ssl_certificate /home/admin/node1.tp1.b2.crt;
                ssl_certificate_key /home/admin/node1.tp1.b2.key;
                server_name node1.tp1.b2;

                location / {
                        return 301 /site1;
                }

                location /site1 {
                        alias /srv/site1/index.html;
                }

                location /site2 {
                        alias /srv/site2/index.html;
                }
        }
}
```

Pour générer des certificats, on utilise la commande `penssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt`

Vérification que la machine `node2` communique avec `node1` : 
```bash
[admin@node2 ~]$ curl http://node1.tp1.b2/site1

coucou !!

FROM SITE 1
[admin@node2 ~]$ curl -k https://node1.tp1.b2/site1

coucou !!

FROM SITE 1
[admin@node2 ~]$ curl http://node1.tp1.b2/site2
coucou !!
FROM SITE2
[admin@node2 ~]$ curl -k https://node1.tp1.b2/site2
coucou !!
FROM SITE2
```
Le `node2` communique donc en `HTTP` et `HTTPS` avec le `node1` et récupère donc la page HTML présente sur `node1`
On utilise l'option `-k` parce que les certificats ne sont pas reconnus comme fiable, cette option cache donc l'erreur de sécurité qui apparait.

## 2. Script de sauvegarde

Je crée l'utilisateur `backup` qui permettra de réaliser la sauvegarde :
```bash
[admin@node1 ~]$ sudo useradd backup
[sudo] password for admin:
[admin@node1 ~]$
```
Je l'ajoute au groupe web pour qu'il ait les autorisations nécessaires à la copie des dossiers : 
```bash
[admin@node1 ~]$ sudo usermod -a -G web backup
[admin@node1 ~]$ groups backup
backup : backup web
```

Je crée ensuite le script de sauvegarde dans un fichier nommé `tp1_backup.sh` avec les bonnes autorisations : 
```bash
[admin@node1 ~]$ sudo -u backup vim /home/backup/tp1_backup.sh
[admin@node1 ~]$ sudo -u backup ls -al /home/backup/
total 20
drwx------. 2 backup backup  99 Sep 25 12:08 .
drwxr-xr-x. 6 root   root    59 Sep 25 12:06 ..
-rw-r--r--. 1 backup backup  18 Apr  1 04:17 .bash_logout
-rw-r--r--. 1 backup backup 193 Apr  1 04:17 .bash_profile
-rw-r--r--. 1 backup backup 231 Apr  1 04:17 .bashrc
-rw-r--r--. 1 backup backup  11 Sep 25 12:08 tp1_backup.sh
-rw-------. 1 backup backup 636 Sep 25 12:08 .viminfo
[admin@node1 ~]$ sudo chmod 744 /home/backup/tp1_backup.sh
[admin@node1 ~]$ sudo -u backup ls -al /home/backup/
total 20
drwx------. 2 backup backup  99 Sep 25 12:08 .
drwxr-xr-x. 6 root   root    59 Sep 25 12:06 ..
-rw-r--r--. 1 backup backup  18 Apr  1 04:17 .bash_logout
-rw-r--r--. 1 backup backup 193 Apr  1 04:17 .bash_profile
-rw-r--r--. 1 backup backup 231 Apr  1 04:17 .bashrc
-rwxr--r--. 1 backup backup  11 Sep 25 12:08 tp1_backup.sh
-rw-------. 1 backup backup 636 Sep 25 12:08 .viminfo
```

Le script réalisé est le suivant : 
```sh
[admin@node1 ~]$ sudo cat /home/backup/tp1_backup.sh
#!/bin/bash
heure=$(date +%H%M);
jour=$(date +%Y%m%d);
parametre=$1;
nom_site=${parametre##*/};
nom_fichier=$nom_site'_'$jour'_'$heure;

if test -f /home/backup/lock_$nom_site;
then
        echo "Script déja en cours d'exécution !";
        exit 22;
fi


touch /home/backup/lock_$nom_site;

tar Pzcvf /home/backup/$nom_fichier.tar.gz $parametre

nombre_fichiers=$(ls /home/backup | grep -c $nom_site);

if test $nombre_fichiers -ge 9;
then
        suppression=$(ls /home/backup/ | grep $nom_site | sort | head -n 2 | tail -n 1);
        rm -rf /home/backup/$suppression;
fi

rm /home/backup/lock_$nom_site;
```
Si l'on souhaite lancer la sauvegarde manuellement, la comamnde à passer est : 
```bash
[admin@node1 ~]$ sudo bash /home/backup/tp1_backup.sh /srv/site1
/srv/site1/
/srv/site1/index.html
/srv/site1/lost+found/
```

Afin d'automaiser le processus de sauvegarde, on crée une crontab. Pour cela, il faut éditer le fichier de configuration avec la commande `sudo vim /etc/crontab`, ce qui donne :
```bash
[admin@node1 ~]$ cat /etc/crontab
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
  0  *  *  *  * backup     bash /home/backup/tp1_backup.sh /srv/site1
  0  *  *  *  * backup     bash /home/backup/tp1_backup.sh /srv/site2
```

Le script s'exécutera donc toutes les heures, tous les jours.

Pour décompresser une archive, il faut taper la commande : 
```bash
[admin@node1 ~]$ sudo tar -zxvf /home/backup/site1_20200928_1021.tar.gz -C /
srv/site1/
srv/site1/index.html
srv/site1/lost+found/
```
Cette commande remplace les fichiers dans le dossier d'origine afin de les retrouver dans un état antérieur.

## NETDATA

Afin d'installer `Netdata`, la commande à passer est : `bash <(curl -Ss https://my-netdata.io/kickstart.sh)`.

Cette commande réalise l'installationet la configuration automatique du logiciel. Après cela, il faut autoriser le port `19999`, qui est le port par défaut de `Netdata`. Je tape donc la commande `sudo firewall-cmd --add-port=19999/tcp --permanent` et après, je reload le service avec la commande `sudo firewall-cmd --reload`.

Afin de recevoir des notifications sur Discord lorsque la machine est saturée en RAM par exemple, il est nécessaire de créer un `channel` dans un serveur, dans les paramètre du channel, créer un `WebHook`. Cela va permettre à `Netdata` d'envoyer des messages sous la forme d'un bot. Je modifie le fichier de configuration en tapant la commande `sudo /etc/netdata/edit-config health_alarm_notify.conf`
J'ajoute à la fin de mon fichier de configration cette partie : 
```bash
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/760060798156144651/HkX3Eg0-3eSiXMomrA3CFvyKBI3qK_MUJHPOnq4cYbY2cv2VL8-qdHkQG9rMXY-40_ev"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```
Cela permet donc au bot d'envoyer des messages. Afin de tester le bon fonctionnement, j'ai tapé la commande `stress -c 2 -i 1 -m 1 --vm-bytes 712M -t 60s` afin que pendant 60 secondes, un processus utilise 712 Mo de RAM, ce qui m'a permis de recevoir ces messages : 

![](netdata.PNG)

Nous pouvons donc voir que les notifications de RAM saturée, d'utilisation du swap arrivent bien sur le serveur Discord.

La configuration du service est donc bien fonctionnelle.

![](the_end.gif)
